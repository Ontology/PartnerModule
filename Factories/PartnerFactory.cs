﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PartnerModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Factories
{
    public class PartnerFactory : NotifyPropertyChange
    {
        private Globals globals;
        private object factoryLocker = new object();

        private Dictionary<string, object> newComItem;
        public Dictionary<string, object> NewComItem
        {
            get { return newComItem; }
            set
            {
                newComItem = value;
                RaisePropertyChanged(nameof(NewComItem));
            }
        }

        public async Task<Dictionary<string, object>> GetNewComItem(clsObjectRel communicationToItem, clsLocalConfig localConfig)
        {
            var dict = new Dictionary<string, object>();

            lock (factoryLocker)
            {

                

                if (communicationToItem.ID_RelationType == localConfig.OItem_relationtype_tel.GUID)
                {
                    dict.Add("Type", "Tel");
                }
                else if (communicationToItem.ID_RelationType == localConfig.OItem_relationtype_fax.GUID)
                {
                    dict.Add("Type", "Fax");
                }
                else if (communicationToItem.ID_Parent_Other == localConfig.OItem_type_email_address.GUID)
                {
                    dict.Add("Type", "Email");
                }
                else if (communicationToItem.ID_Parent_Other == localConfig.OItem_type_url.GUID)
                {
                    dict.Add("Type", "Url");
                }

                dict.Add("Id", communicationToItem.ID_Other);
                dict.Add("Value", communicationToItem.Name_Other);
                        

                
            }

            return dict;
        }

        public async Task<clsOntologyItem> CreateJsonCom(SessionFile sessionFile, List<clsObjectRel> communicationToPartners,
            List<clsObjectRel> communicationToEmailAddresses,
            List<clsObjectRel> communicationToPhoneNumbers,
            List<clsObjectRel> communicationToFaxNumbers,
            List<clsObjectRel> communicationToUrls,
            clsLocalConfig localConfig)
        {
            lock(factoryLocker)
            {
                using (sessionFile.StreamWriter)
                {
                    using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                    {
                        jsonWriter.WriteStartArray();
                        communicationToPhoneNumbers.ForEach(phone =>
                        {
                            jsonWriter.WriteStartObject();

                            jsonWriter.WritePropertyName("Type");
                            jsonWriter.WriteValue("Tel");
                            

                            jsonWriter.WritePropertyName("Id");
                            jsonWriter.WriteValue(phone.ID_Other);

                            jsonWriter.WritePropertyName("Value");
                            jsonWriter.WriteValue(phone.Name_Other);

                            jsonWriter.WriteEndObject();
                        });

                        communicationToFaxNumbers.ForEach(phone =>
                        {
                            jsonWriter.WriteStartObject();

                            jsonWriter.WritePropertyName("Type");
                            jsonWriter.WriteValue("Fax");


                            jsonWriter.WritePropertyName("Id");
                            jsonWriter.WriteValue(phone.ID_Other);

                            jsonWriter.WritePropertyName("Value");
                            jsonWriter.WriteValue(phone.Name_Other);

                            jsonWriter.WriteEndObject();
                        });

                        communicationToEmailAddresses.ForEach(email =>
                        {
                            jsonWriter.WriteStartObject();

                            jsonWriter.WritePropertyName("Type");
                            jsonWriter.WriteValue("Email");

                            jsonWriter.WritePropertyName("Id");
                            jsonWriter.WriteValue(email.ID_Other);

                            jsonWriter.WritePropertyName("Value");
                            jsonWriter.WriteValue(email.Name_Other);

                            jsonWriter.WriteEndObject();
                        });

                        communicationToUrls.ForEach(url =>
                        {
                            jsonWriter.WriteStartObject();

                            jsonWriter.WritePropertyName("Type");
                            jsonWriter.WriteValue("Url");

                            jsonWriter.WritePropertyName("Id");
                            jsonWriter.WriteValue(url.ID_Other);

                            jsonWriter.WritePropertyName("Value");
                            jsonWriter.WriteValue(url.Name_Other);

                            jsonWriter.WriteEndObject();
                        });


                        jsonWriter.WriteEndArray();
                    }
                }
            }

            return localConfig.Globals.LState_Success.Clone();
        }

        public async Task<dynamic> CreatePartnerList(List<clsObjectRel> partnersOfRefLeftRight,
            List<clsObjectRel> partnersOfRefRightLeft,
            List<clsObjectRel> partnersToAddresses,
            List<clsObjectAtt> addressStrasse,
            List<clsObjectAtt> addressPostfach,
            List<clsObjectRel> plzs,
            List<clsObjectRel> orte,
            List<clsObjectRel> lands,
            List<clsObjectRel> communicationToPartners,
            List<clsObjectRel> communicationToEmailAddresses,
            List<clsObjectRel> communicationToPhoneNumbers,
            List<clsObjectRel> communicationToFaxNumbers,
            List<clsObjectRel> communicationToUrls, List<clsObjectRel> tagsToTaggingSources, List<clsObjectRel> tagsToTag)
        {

            
            var partnersToRef = partnersOfRefLeftRight.Select(partnerOfRef => new RefToPartner
            {
                IdDirection = globals.Direction_LeftRight.GUID,
                IdRef = partnerOfRef.ID_Other,
                NameRef = partnerOfRef.Name_Other,
                Partner = new Partner
                {
                    Id = partnerOfRef.ID_Object,
                    Name = partnerOfRef.Name_Object
                }
            }).ToList();

            partnersToRef.AddRange(partnersOfRefRightLeft.Select(partnerOfRef => new RefToPartner
            {
                IdDirection = globals.Direction_RightLeft.GUID,
                IdRef = partnerOfRef.ID_Object,
                NameRef = partnerOfRef.Name_Object,
                Partner = new Partner
                {
                    Id = partnerOfRef.ID_Other,
                    Name = partnerOfRef.Name_Other,
                }
            }));

            partnersToRef.ForEach(partner =>
            {
                partner.Partner.Address = (from partnerToAddress in partnersToAddresses.Where(partAddr => partAddr.ID_Object == partner.Partner.Id)
                                           join strasse in addressStrasse on partnerToAddress.ID_Other equals strasse.ID_Object into strassen
                                           from strasse in strassen.DefaultIfEmpty()
                                           join postfach in addressPostfach on partnerToAddress.ID_Other equals postfach.ID_Object into postfaecher
                                           from postfach in postfaecher.DefaultIfEmpty()
                                           join plzItem in plzs on partnerToAddress.ID_Other equals plzItem.ID_Object
                                           join ortItem in orte on partnerToAddress.ID_Other equals ortItem.ID_Object
                                           join land in lands on ortItem.ID_Other equals land.ID_Object
                                           select new Address
                                           {
                                               Id = partnerToAddress.ID_Other,
                                               Name = partnerToAddress.Name_Other,
                                               IdStrasse = strasse != null ? strasse.ID_Attribute : null,
                                               NameStrasse = strasse != null ? strasse.Val_String : null,
                                               IdPostfach = postfach != null ? postfach.ID_Attribute : null,
                                               NamePostfach = postfach != null ? postfach.Val_String : null,
                                               IdPlz = plzItem.ID_Other,
                                               Plz = plzItem.Name_Other,
                                               IdOrt = ortItem.ID_Other,
                                               NameOrt = ortItem.Name_Other,
                                               IdLand = land.ID_Other,
                                               NameLand = land.Name_Other
                                           }).FirstOrDefault();

                partner.Partner.EmailAddress = (from com in communicationToPartners.Where(com => com.ID_Other == partner.Partner.Id)
                                      join emailAddress in communicationToEmailAddresses on com.ID_Object equals emailAddress.ID_Object
                                      select new EmailAddress
                                      {
                                          Id = emailAddress.ID_Other,
                                          Name = emailAddress.Name_Other
                                      }).ToList();

                partner.Partner.PhoneNumbers = (from com in communicationToPartners.Where(com => com.ID_Other == partner.Partner.Id)
                              join phoneNumber in communicationToPhoneNumbers on com.ID_Object equals phoneNumber.ID_Object
                              select new PhoneNumber
                              {
                                  Id = phoneNumber.ID_Other,
                                  Name = phoneNumber.Name_Other
                              }).ToList();

                partner.Partner.FaxNumbers = (from com in communicationToPartners.Where(com => com.ID_Other == partner.Partner.Id)
                                              join phoneNumber in communicationToFaxNumbers on com.ID_Object equals phoneNumber.ID_Object
                                              select new PhoneNumber
                                              {
                                                  Id = phoneNumber.ID_Other,
                                                  Name = phoneNumber.Name_Other
                                              }).ToList();

                partner.Partner.Urls = (from com in communicationToPartners.Where(com => com.ID_Other == partner.Partner.Id)
                            join url in communicationToUrls on com.ID_Object equals url.ID_Object
                            select new Url
                            {
                                IdUrl = url.ID_Other,
                                NameUrl = url.Name_Other
                            }).ToList();

                
            });

            var tags = tagsToTag.GroupBy(tagItm => new { IdTag = tagItm.ID_Other, NameTag = tagItm.Name_Other }).Select(tagItm => new
            {
                IdTag = tagItm.Key.IdTag,
                NameTag = tagItm.Key.NameTag
            }).OrderBy(tag => tag.NameTag);

            var tagsToPartnerRef = from tagToSource in tagsToTaggingSources
                                   join tag in tagsToTag on tagToSource.ID_Object equals tag.ID_Object
                                   join partnerToRef in partnersToRef on tagToSource.ID_Other equals partnerToRef.Partner.Id
                                   select new { IdTag = tag.ID_Other, IdPartner = partnerToRef.Partner.Id };

            var partnersToRefWithTag = partnersToRef.Select(partnerToRef => new { partnerToRef, Tags = tagsToPartnerRef.Where(tag => tag.IdPartner == partnerToRef.Partner.Id).Select(tag => tag.IdTag) });



            return new { TagItems = tags, PartnerRefs = partnersToRefWithTag };
        }

        public PartnerFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
