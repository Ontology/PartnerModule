﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Base;
using PartnerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PartnerModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object serviceLocker = new object();

        private clsTransaction transaction;
        private clsRelationConfig relationConfig;

        private clsOntologyItem resultAddressSave;
        public clsOntologyItem ResultAddressSave
        {
            get { return resultAddressSave; }
            set
            {
                resultAddressSave = value;
                RaisePropertyChanged(nameof(ResultAddressSave));
            }
        }

        private clsOntologyItem resultKommunikationsangabenSave;
        public clsOntologyItem ResultKommunikationsangabenSave
        {
            get { return resultKommunikationsangabenSave; }
            set
            {
                resultKommunikationsangabenSave = value;
                RaisePropertyChanged(nameof(ResultKommunikationsangabenSave));
            }
        }

        private clsOntologyItem resultNatuerlichePersonSave;
        public clsOntologyItem ResultNatuerlichePersonSave
        {
            get { return resultNatuerlichePersonSave; }
            set
            {
                resultNatuerlichePersonSave = value;
                RaisePropertyChanged(nameof(ResultNatuerlichePersonSave));
            }
        }

        private clsOntologyItem resultOrtAndOrtSave;
        public clsOntologyItem ResultOrtAndOrtSave
        {
            get { return resultOrtAndOrtSave; }
            set
            {
                resultOrtAndOrtSave = value;
                RaisePropertyChanged(nameof(ResultOrtAndOrtSave));
            }
        }

        private clsOntologyItem resultKommunikationsangabenData;
        public clsOntologyItem ResultKommunikationsangabenData
        {
            get { return resultKommunikationsangabenData; }
            set
            {
                resultKommunikationsangabenData = value;
                RaisePropertyChanged(nameof(ResultKommunikationsangabenData));
            }
        }

        public clsOntologyItem OItemOrt { get; set; }

        public clsOntologyItem OItemPlz { get; set; }

        public clsOntologyItem OItemLand { get; set; }

        public clsOntologyItem OItemAddress { get; set; }

        public clsOntologyItem OItemKommunikationsangaben { get; set; }

        public clsOntologyItem OItemNatuerlichePerson { get; set; }

        public clsOntologyItem OItemGeburtsort { get; set; }

        public clsOntologyItem OItemGeschlecht { get; set; }

        public clsOntologyItem OItemFamilienstand { get; set; }

        public clsOntologyItem OItemSozialversicherungsnummer { get; set; }

        public clsOntologyItem OItemETin { get; set; }

        public clsOntologyItem OItemINr { get; set; }

        public clsOntologyItem OItemSteuernummer { get; set; }

        private List<clsOntologyItem> partnerList;

        public clsObjectRel KomItemRel { get; private set; }

        public async Task<ServiceResult> GetPartnersSimpleByRef(clsOntologyItem refItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var serviceResult = new ServiceResult(localConfig.OItem_attribute_straße.GUID,
                localConfig.OItem_attribute_postfach.GUID,
                localConfig.OItem_relationtype_tel.GUID,
                localConfig.OItem_relationtype_fax.GUID)
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var refOItem = refItem;

            var searchResult1 = GetPartners(refOItem);

            if (searchResult1.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }
            serviceResult.PartnersOfRefLeftRight = searchResult1.ResultList;
            serviceResult.PartnersOfRefRightLeft = searchResult1.ResultListRightLeft;

            return serviceResult;
        }

        public async Task<ServiceResult> GetAddresses(List<clsOntologyItem> addressesRaw)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var serviceResult = new ServiceResult(localConfig.OItem_attribute_straße.GUID,
                localConfig.OItem_attribute_postfach.GUID,
                localConfig.OItem_relationtype_tel.GUID,
                localConfig.OItem_relationtype_fax.GUID)
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchResult3 = GetAddressAttributes(addressesRaw);

            if (searchResult3.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.AddressAttributes = searchResult3.ResultList;

            var searchResult4 = GetPlz(addressesRaw);

            if (searchResult4.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Plzs = searchResult4.ResultList;

            var searchResult5 = GetOrte(addressesRaw);

            if (searchResult5.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Orte = searchResult5.ResultList;

            var searchResult6 = GetLands(serviceResult.Orte);

            if (searchResult6.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Lands = searchResult6.ResultList;

            return serviceResult;
        }

        public async Task<ServiceResult> GetPartnersByRef(clsOntologyItem refItem)
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var serviceResult = new ServiceResult(localConfig.OItem_attribute_straße.GUID,
                localConfig.OItem_attribute_postfach.GUID,
                localConfig.OItem_relationtype_tel.GUID,
                localConfig.OItem_relationtype_fax.GUID)
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var refOItem = refItem;

            if (refItem.GUID_Parent != localConfig.OItem_type_partner.GUID)
            {
                var searchResult1 = GetPartners(refOItem);

                if (searchResult1.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                    return serviceResult;
                }
                serviceResult.PartnersOfRefLeftRight = searchResult1.ResultList;
                serviceResult.PartnersOfRefRightLeft = searchResult1.ResultListRightLeft;

                lock (serviceLocker)
                {
                    partnerList = serviceResult.PartnersOfRefLeftRight.Select(partnerRef => new clsOntologyItem
                    {
                        GUID = partnerRef.ID_Object,
                        Name = partnerRef.Name_Object,
                        GUID_Parent = partnerRef.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).ToList();

                    partnerList.AddRange(serviceResult.PartnersOfRefRightLeft.Select(partnerRef => new clsOntologyItem
                    {
                        GUID = partnerRef.ID_Other,
                        Name = partnerRef.Name_Other,
                        GUID_Parent = partnerRef.ID_Parent_Other,
                        Type = localConfig.Globals.Type_Other
                    }));
                }
            }
            else
            {
                lock (serviceLocker)
                {
                    partnerList = new List<clsOntologyItem>()
                    {
                        refItem
                    };
                }
            }
            
            

            var searchResult2 = GetAddress(partnerList);

            if (searchResult2.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.PartnersToAddresses = searchResult2.ResultList;

            var searchResult3 = GetAddressAttributes(serviceResult.PartnersToAddresses);

            if (searchResult3.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.AddressAttributes = searchResult3.ResultList;

            var searchResult4 = GetPlz(serviceResult.PartnersToAddresses);

            if (searchResult4.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Plzs = searchResult4.ResultList;

            var searchResult5 = GetOrte(serviceResult.PartnersToAddresses);

            if (searchResult5.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Orte = searchResult5.ResultList;

            var searchResult6 = GetLands(serviceResult.Orte);

            if (searchResult6.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.Lands = searchResult6.ResultList;

            var searchResult7 = GetCommunication(partnerList);

            if (searchResult7.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.CommunicationToPartners = searchResult7.ResultList;

            var searchResult8 = GetCommunicationToEmailAddresses(serviceResult.CommunicationToPartners);

            if (searchResult8.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.CommunicationToEmailAddresses = searchResult8.ResultList;

            var searchResult9 = GetCommunicationToPhoneNumbers(serviceResult.CommunicationToPartners);

            if (searchResult9.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.CommunicationToPhoneNumbers = searchResult9.ResultList;

            var searchResult10 = GetCommunicationToUrls(serviceResult.CommunicationToPartners);

            if (searchResult10.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.CommunicationToUrls = searchResult10.ResultList;

            var searchResult11 = GetPersonaldata(partnerList);

            if (searchResult11.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }

            serviceResult.PersonalDataToPartner = searchResult11.ResultList;

            var searchResult12 = GetPersonaldataToAttributes(serviceResult.PersonalDataToPartner);

            if (searchResult12.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }
            else
            {
                
                serviceResult.PersonalDataToGeburtsdatum = searchResult12.ResultList.Where(rel => rel.ID_AttributeType == localConfig.OItem_attribute_geburtsdatum.GUID).ToList();
                serviceResult.PersonalDataToNachname = searchResult12.ResultList.Where(rel => rel.ID_AttributeType == localConfig.OItem_attribute_nachname.GUID).ToList();
                serviceResult.PersonalDataToTodesdatum = searchResult12.ResultList.Where(rel => rel.ID_AttributeType == localConfig.OItem_attribute_todesdatum.GUID).ToList();
                serviceResult.PersonalDataToVorname = searchResult12.ResultList.Where(rel => rel.ID_AttributeType == localConfig.OItem_attribute_vorname.GUID).ToList();
            }

            var searchResult13 = GetPersonaldataRels(serviceResult.PersonalDataToPartner);

            if (searchResult13.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                serviceResult.Result = localConfig.Globals.LState_Error.Clone();
                return serviceResult;
            }
            else
            {
                serviceResult.PersonalDataToETin = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_etin.GUID).ToList();
                serviceResult.PersonalDataToFamilienstand = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_familienstand.GUID).ToList();
                serviceResult.PersonalDataToGeschlecht = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_geschlecht.GUID).ToList();
                serviceResult.PersonalDataToIdentifikationsnummer = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_identifkationsnummer__idnr_.GUID).ToList();
                serviceResult.PersonalDataToSozialversicherungsnummer = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_sozialversicherungsnummer.GUID).ToList();
                serviceResult.PersonalDataToSteuernummer = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_steuernummer.GUID).ToList();
                serviceResult.PersonalDataToGeburtsort = searchResult13.ResultList.Where(rel => rel.ID_Parent_Other == localConfig.OItem_type_ort.GUID && rel.ID_RelationType == localConfig.OItem_relationtype_geboren_in.GUID).ToList();

            }




            return serviceResult;
        }

        private SearchResult<clsObjectRel> GetPartners(clsOntologyItem refItem)
        {

            var searchResult = new SearchResult<clsObjectRel>()
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var dbReaderLeftRight = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderRightLeft = new OntologyModDBConnector(localConfig.Globals);

            var searchPartners = new List<clsObjectRel> { new clsObjectRel
                {
                    ID_Parent_Object = localConfig.OItem_type_partner.GUID,
                    ID_Other = refItem.GUID,
                    ID_Parent_Other = refItem.GUID_Parent
                }
            };

            var result = dbReaderLeftRight.GetDataObjectRel(searchPartners);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                searchResult.Result = result;
                return searchResult;
            }

            searchResult.ResultList = dbReaderLeftRight.ObjectRels;

            searchPartners = new List<clsObjectRel> { new clsObjectRel
                {
                    ID_Parent_Other = localConfig.OItem_type_partner.GUID,
                    ID_Object = refItem.GUID,
                    ID_Parent_Object = refItem.GUID_Parent
                }
            };

            result = dbReaderRightLeft.GetDataObjectRel(searchPartners);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                searchResult.Result = result;
                return searchResult;
            }

            searchResult.ResultListRightLeft = dbReaderRightLeft.ObjectRels;

            return searchResult;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);
        }


        #region AddressMethods

        private SearchResult<clsObjectRel> GetAddress(List<clsOntologyItem> itemList)
        {
            var searchResult = new SearchResult<clsObjectRel>()
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchAddress = partnerList.Select(partner => new clsObjectRel
            {
                ID_Object = partner.GUID,
                ID_RelationType = localConfig.OItem_relationtype_sitz.GUID,
                ID_Parent_Other = localConfig.OItem_type_address.GUID
            }).ToList();

            if (searchAddress.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchAddress);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }


        private SearchResult<clsObjectAtt> GetAddressAttributes(List<clsObjectRel> partnersToAddresses)
        {
            var searchResult = new SearchResult<clsObjectAtt>()
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchAttributes = partnersToAddresses.Select(partnerToAddress => new clsObjectAtt
            {
                ID_Object = partnerToAddress.ID_Other,
                ID_AttributeType = localConfig.OItem_attribute_postfach.GUID
            }).ToList();

            searchAttributes.AddRange(partnersToAddresses.Select(partnerToAddress => new clsObjectAtt
            {
                ID_Object = partnerToAddress.ID_Other,
                ID_AttributeType = localConfig.OItem_attribute_straße.GUID
            }));

            if (searchAttributes.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectAtt(searchAttributes);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjAtts;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectAtt>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectAtt> GetAddressAttributes(List<clsOntologyItem> addresses)
        {
            var searchResult = new SearchResult<clsObjectAtt>()
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchAttributes = addresses.Select(partnerToAddress => new clsObjectAtt
            {
                ID_Object = partnerToAddress.GUID,
                ID_AttributeType = localConfig.OItem_attribute_postfach.GUID
            }).ToList();

            searchAttributes.AddRange(addresses.Select(partnerToAddress => new clsObjectAtt
            {
                ID_Object = partnerToAddress.GUID,
                ID_AttributeType = localConfig.OItem_attribute_straße.GUID
            }));

            if (searchAttributes.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectAtt(searchAttributes);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjAtts;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectAtt>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectRel> GetPlz(List<clsObjectRel> partnersToAddresses)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchPlzs = partnersToAddresses.Select(partnerToAddress => new clsObjectRel
            {
                ID_Object = partnerToAddress.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_postleitzahl.GUID
            }).ToList();

            if (searchPlzs.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchPlzs);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectRel> GetPlz(List<clsOntologyItem> addresses)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchPlzs = addresses.Select(partnerToAddress => new clsObjectRel
            {
                ID_Object = partnerToAddress.GUID,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_postleitzahl.GUID
            }).ToList();

            if (searchPlzs.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchPlzs);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectRel> GetOrte(List<clsObjectRel> partnersToAddresses)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var searchOrte = partnersToAddresses.Select(partnerToAddress => new clsObjectRel
            {
                ID_Object = partnerToAddress.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_ort.GUID
            }).ToList();

            if (searchOrte.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchOrte);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectRel> GetOrte(List<clsOntologyItem> addresses)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };
            var searchOrte = addresses.Select(partnerToAddress => new clsObjectRel
            {
                ID_Object = partnerToAddress.GUID,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_ort.GUID
            }).ToList();

            if (searchOrte.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchOrte);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;
        }

        private SearchResult<clsObjectRel> GetLands(List<clsObjectRel> orte)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchLands = orte.Select(ort => new clsObjectRel
            {
                ID_Object = ort.ID_Other,
                ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_land.GUID
            }).ToList();

            if (searchLands.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchLands);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;
        }

        #endregion

        #region CommunicationMethods

        private SearchResult<clsObjectRel> GetCommunication(List<clsOntologyItem> partners)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchCommunication = partners.Select(partner => new clsObjectRel
            {
                ID_Other = partner.GUID,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Object = localConfig.OItem_type_kommunikationsangaben.GUID
            }).ToList();

            
            if (searchCommunication.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchCommunication);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        private SearchResult<clsObjectRel> GetCommunicationToEmailAddresses(List<clsObjectRel> communicationToPartners)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchComsToPartners = communicationToPartners.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_type_email_address.GUID
            }).ToList();

           
            if (searchComsToPartners.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchComsToPartners);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        private SearchResult<clsObjectRel> GetCommunicationToPhoneNumbers(List<clsObjectRel> communicationToPartners)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchComsToPhone = communicationToPartners.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_fax.GUID,
                ID_Parent_Object = localConfig.OItem_type_telefonnummer.GUID
            }).ToList();

            searchComsToPhone.AddRange(communicationToPartners.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_tel.GUID,
                ID_Parent_Other = localConfig.OItem_type_telefonnummer.GUID
            }));


            if (searchComsToPhone.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchComsToPhone);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        private SearchResult<clsObjectRel> GetCommunicationToUrls(List<clsObjectRel> communicationToPartners)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchComsToPartners = communicationToPartners.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = localConfig.OItem_type_url.GUID
            }).ToList();


            if (searchComsToPartners.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchComsToPartners);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                }
                searchResult.ResultList =dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        #endregion

        #region Personal

        private SearchResult<clsObjectRel> GetPersonaldata(List<clsOntologyItem> partners)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchPersonalData = partners.Select(partner => new clsObjectRel
            {
                ID_Other = partner.GUID,
                ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                ID_Parent_Object = localConfig.OItem_type_nat_rliche_person.GUID
            }).ToList();


            if (searchPersonalData.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchPersonalData);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        private SearchResult<clsObjectAtt> GetPersonaldataToAttributes(List<clsObjectRel> personalData)
        {
            var searchResult = new SearchResult<clsObjectAtt>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchPersonalDataAttribs = personalData.Select(personal => new clsObjectAtt
            {
                ID_Object = personal.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_geburtsdatum.GUID
            }).ToList();

            searchPersonalDataAttribs.AddRange(personalData.Select(personal => new clsObjectAtt
            {
                ID_Object = personal.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_nachname.GUID
            }));

            searchPersonalDataAttribs.AddRange(personalData.Select(personal => new clsObjectAtt
            {
                ID_Object = personal.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_todesdatum.GUID
            }));

            searchPersonalDataAttribs.AddRange(personalData.Select(personal => new clsObjectAtt
            {
                ID_Object = personal.ID_Object,
                ID_AttributeType = localConfig.OItem_attribute_vorname.GUID
            }));

            if (searchPersonalDataAttribs.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectAtt(searchPersonalDataAttribs);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                    return searchResult;
                }

                searchResult.ResultList = dbReader.ObjAtts;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectAtt>();
            }

            return searchResult;

        }

        private SearchResult<clsObjectRel> GetPersonaldataRels(List<clsObjectRel> personalData)
        {
            var searchResult = new SearchResult<clsObjectRel>
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var searchPersonalDataRel = personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_isinstate.GUID,
                ID_Parent_Other = localConfig.OItem_type_familienstand.GUID
            }).ToList();

            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_belonging.GUID,
                ID_Parent_Other = localConfig.OItem_type_geschlecht.GUID
            }));

            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_has.GUID,
                ID_Parent_Other = localConfig.OItem_type_identifkationsnummer__idnr_.GUID
            }));

            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_has.GUID,
                ID_Parent_Other = localConfig.OItem_type_sozialversicherungsnummer.GUID
            }));

            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_has.GUID,
                ID_Parent_Other = localConfig.OItem_type_steuernummer.GUID
            }));

            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_has.GUID,
                ID_Parent_Other = localConfig.OItem_type_etin.GUID
            }));


            searchPersonalDataRel.AddRange(personalData.Select(comToPartner => new clsObjectRel
            {
                ID_Object = comToPartner.ID_Object,
                ID_RelationType = localConfig.OItem_relationtype_geboren_in.GUID,
                ID_Parent_Other = localConfig.OItem_type_ort.GUID
            }));

            if (searchPersonalDataRel.Any())
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);
                var result = dbReader.GetDataObjectRel(searchPersonalDataRel);

                if (result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    searchResult.Result = result;
                }
                searchResult.ResultList = dbReader.ObjectRels;
            }
            else
            {
                searchResult.ResultList = new List<clsObjectRel>();
            }

            return searchResult;

        }

        #endregion

        public async Task<clsOntologyItem> SaveNatuerlichPersonData(clsOntologyItem oItemNatuerlichePerson, clsOntologyItem oItemRel)
        {
            if (oItemNatuerlichePerson == null ||
                oItemRel == null ||
                oItemNatuerlichePerson.GUID_Parent != localConfig.OItem_type_nat_rliche_person.GUID)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            if (!(oItemRel.GUID_Parent == localConfig.OItem_type_geschlecht.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_familienstand.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_sozialversicherungsnummer.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_etin.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_identifkationsnummer__idnr_.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_steuernummer.GUID ||
                  oItemRel.GUID_Parent == localConfig.OItem_type_ort.GUID))
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            if (oItemRel.GUID_Parent == localConfig.OItem_type_geschlecht.GUID)
            {
                OItemGeschlecht = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_familienstand.GUID)
            {
                OItemFamilienstand = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_sozialversicherungsnummer.GUID)
            {
                OItemSozialversicherungsnummer = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_etin.GUID)
            {
                OItemETin = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_identifkationsnummer__idnr_.GUID)
            {
                OItemINr = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_steuernummer.GUID)
            {
                OItemSteuernummer = null;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_ort.GUID)
            {
                OItemGeburtsort = null;
            }

            var relationType = oItemRel.GUID_Parent == localConfig.OItem_type_geschlecht.GUID ?
                    localConfig.OItem_relationtype_belonging :
                    oItemRel.GUID_Parent == localConfig.OItem_type_familienstand.GUID ?
                            localConfig.OItem_relationtype_isinstate :
                            oItemRel.GUID_Parent == localConfig.OItem_type_sozialversicherungsnummer.GUID ?
                                localConfig.OItem_relationtype_has :
                                oItemRel.GUID_Parent == localConfig.OItem_type_identifkationsnummer__idnr_.GUID ?
                                    localConfig.OItem_relationtype_has :
                                    oItemRel.GUID_Parent == localConfig.OItem_type_etin.GUID ?
                                        localConfig.OItem_relationtype_has :
                                        oItemRel.GUID_Parent == localConfig.OItem_type_steuernummer.GUID ?
                                            localConfig.OItem_relationtype_has :
                                            oItemRel.GUID_Parent == localConfig.OItem_type_ort.GUID ?
                                                localConfig.OItem_relationtype_geboren_in :
                                                null;

            if (relationType == null)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            var rel = relationConfig.Rel_ObjectRelation(oItemNatuerlichePerson, oItemRel, relationType, false);

            transaction.ClearItems();

            var result = transaction.do_Transaction(rel, boolRemoveAll: true);

            if (oItemRel.GUID_Parent == localConfig.OItem_type_geschlecht.GUID)
            {
                OItemGeschlecht = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_familienstand.GUID)
            {
                OItemFamilienstand = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_sozialversicherungsnummer.GUID)
            {
                OItemSozialversicherungsnummer = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_identifkationsnummer__idnr_.GUID)
            {
                OItemINr = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_etin.GUID)
            {
                OItemETin = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_steuernummer.GUID)
            {
                OItemSteuernummer = oItemRel;
            }
            else if (oItemRel.GUID_Parent == localConfig.OItem_type_ort.GUID)
            {
                OItemGeburtsort = oItemRel;
            }

            ResultNatuerlichePersonSave = result;
            return result;
        }

        public async Task<clsOntologyItem> SaveKommunikationsAngabenData(clsOntologyItem oItemKommunikationsAngaben, clsOntologyItem oItemRel, clsOntologyItem oItemrelationType)
        {
            if (oItemKommunikationsAngaben == null ||
                oItemRel == null ||
                oItemrelationType == null)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            if (!((oItemRel.GUID_Parent == localConfig.OItem_type_telefonnummer.GUID && oItemrelationType.GUID == localConfig.OItem_relationtype_tel.GUID) ||
                  (oItemRel.GUID_Parent == localConfig.OItem_type_telefonnummer.GUID && oItemrelationType.GUID == localConfig.OItem_relationtype_fax.GUID) ||
                  (oItemRel.GUID_Parent == localConfig.OItem_type_email_address.GUID && oItemrelationType.GUID == localConfig.OItem_relationtype_contains.GUID) ||
                  (oItemRel.GUID_Parent == localConfig.OItem_type_url.GUID && oItemrelationType.GUID == localConfig.OItem_relationtype_contains.GUID)))
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            var rel = relationConfig.Rel_ObjectRelation(oItemKommunikationsAngaben, oItemRel, oItemrelationType, false);

            var result = transaction.do_Transaction(rel);

            rel.Name_Other = oItemRel.Name;
            KomItemRel = rel;
            ResultKommunikationsangabenData = result;
            return result;
        }

        public async Task<clsOntologyItem> SavePLZOrOrt(clsOntologyItem oItemAddress, clsOntologyItem oItemPLZOrOrt)
        {
            if (oItemAddress == null || 
                oItemPLZOrOrt == null || 
                oItemAddress.GUID_Parent != localConfig.OItem_type_address.GUID)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            if (!(oItemPLZOrOrt.GUID_Parent == localConfig.OItem_type_postleitzahl.GUID ||
                 oItemPLZOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID))
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            
            OItemOrt = null;
            OItemPlz = null;
            OItemLand = null;
            

            var rel = relationConfig.Rel_ObjectRelation(oItemAddress, oItemPLZOrOrt, localConfig.OItem_relationtype_located_in, false);

            transaction.ClearItems();

            var result = transaction.do_Transaction(rel, boolRemoveAll: true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return result;
            }

            if (oItemPLZOrOrt.GUID_Parent == localConfig.OItem_type_postleitzahl.GUID)
            {
                OItemPlz = oItemPLZOrOrt;
                result = GetOrtOfPlzOrPlzOfOrt(oItemPLZOrOrt);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    if (result.OList_Rel.Count == 1)
                    {
                        var ort = result.OList_Rel.FirstOrDefault();

                        if (ort == null)
                        {
                            result = localConfig.Globals.LState_Error.Clone();
                        }
                        else
                        {

                            rel = relationConfig.Rel_ObjectRelation(oItemAddress, ort, localConfig.OItem_relationtype_located_in, false);
                            result = transaction.do_Transaction(rel, boolRemoveAll: true);

                            if (result.GUID == localConfig.Globals.LState_Error.GUID)
                            {
                                transaction.rollback();
                            }

                            if (result.GUID == localConfig.Globals.LState_Success.GUID)
                            {
                                OItemOrt = ort;
                            }
                        }
                    }
                    
                    
                }
                
            }
            else if (oItemPLZOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID)
            {
                OItemOrt = oItemPLZOrOrt;

                if (result.OList_Rel.Count == 1)
                {
                    var plz = result.OList_Rel.FirstOrDefault();

                    if (plz == null)
                    {
                        result = localConfig.Globals.LState_Error.Clone();
                    }
                    else
                    {

                        rel = relationConfig.Rel_ObjectRelation(oItemAddress, plz, localConfig.OItem_relationtype_located_in, false);
                        result = transaction.do_Transaction(rel, boolRemoveAll: true);

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                        }

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            OItemPlz = plz;
                        }
                    }
                }
                
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID && OItemOrt != null)
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var searchLand = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = OItemOrt.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                        ID_Parent_Other = localConfig.OItem_type_land.GUID
                    }
                };

                result = dbReader.GetDataObjectRel(searchLand);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    OItemLand = dbReader.ObjectRels.Select(landRel => new clsOntologyItem
                    {
                        GUID = landRel.ID_Other,
                        Name = landRel.Name_Other,
                        GUID_Parent = landRel.ID_Parent_Other,
                        Type = localConfig.Globals.Type_Object
                    }).First();
                }
            }

            ResultOrtAndOrtSave = result;
            return result;

        }

        private clsOntologyItem GetOrtOfPlzOrPlzOfOrt(clsOntologyItem plzOrOrt)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);

            var searchOrtOrPlz = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = plzOrOrt.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                    ID_Parent_Other = localConfig.OItem_type_ort.GUID
                }
            };

            if (plzOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID)
            {
                searchOrtOrPlz = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = plzOrOrt.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_located_in.GUID,
                        ID_Parent_Object = localConfig.OItem_type_postleitzahl.GUID
                    }
                };
            }

            var result = dbReader.GetDataObjectRel(searchOrtOrPlz);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var ortOrPlz = dbReader.ObjectRels.Select(relOrtOrPlz => new clsOntologyItem
                {
                    GUID = plzOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID ? relOrtOrPlz.ID_Object : relOrtOrPlz.ID_Other,
                    Name = plzOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID ? relOrtOrPlz.Name_Object : relOrtOrPlz.Name_Other,
                    GUID_Parent = plzOrOrt.GUID_Parent == localConfig.OItem_type_ort.GUID ? relOrtOrPlz.ID_Parent_Object : relOrtOrPlz.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).FirstOrDefault();

                result.OList_Rel = new List<clsOntologyItem>
                {
                    ortOrPlz
                };
            }
            else
            {
                result = localConfig.Globals.LState_Error.Clone();
            }

            return result;
        }

        public async Task<clsOntologyItem> SaveAddress(clsOntologyItem oItemPartner)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            clsOntologyItem oItemAddress = null;

            lock(serviceLocker)
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var searchAddressOfPartner = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Object = oItemPartner.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_sitz.GUID,
                        ID_Parent_Other = localConfig.OItem_type_address.GUID
                    }
                };

                result = dbReader.GetDataObjectRel(searchAddressOfPartner);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    oItemAddress = dbReader.ObjectRels.Select(addrRel => new clsOntologyItem
                    {
                        GUID = addrRel.ID_Other,
                        Name = addrRel.Name_Other,
                        GUID_Parent = addrRel.ID_Parent_Other,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();

                    if (oItemAddress == null)
                    {
                        oItemAddress = new clsOntologyItem
                        {
                            GUID = localConfig.Globals.NewGUID,
                            Name = oItemPartner.Name,
                            GUID_Parent = localConfig.OItem_type_address.GUID,
                            Type = localConfig.Globals.Type_Object
                        };

                        transaction.ClearItems();

                        result = transaction.do_Transaction(oItemAddress);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var rel = relationConfig.Rel_ObjectRelation(oItemPartner, oItemAddress, localConfig.OItem_relationtype_sitz);

                            result = transaction.do_Transaction(rel, boolRemoveAll: true);
                        }

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                            oItemAddress = null;
                        }
                        
                    }
                }
            }

            OItemAddress = oItemAddress;
            ResultAddressSave = result;
            return result;
        }

        public async Task<clsOntologyItem> SaveKommunikationsangaben(clsOntologyItem oItemPartner)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            clsOntologyItem oItemKommunikationsangaben = null;

            lock (serviceLocker)
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var searchKommunikationsangabenOfPartner = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemPartner.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                        ID_Parent_Object = localConfig.OItem_type_kommunikationsangaben.GUID
                    }
                };

                result = dbReader.GetDataObjectRel(searchKommunikationsangabenOfPartner);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    oItemKommunikationsangaben = dbReader.ObjectRels.Select(addrRel => new clsOntologyItem
                    {
                        GUID = addrRel.ID_Object,
                        Name = addrRel.Name_Object,
                        GUID_Parent = addrRel.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();

                    if (oItemKommunikationsangaben == null)
                    {
                        oItemKommunikationsangaben = new clsOntologyItem
                        {
                            GUID = localConfig.Globals.NewGUID,
                            Name = oItemPartner.Name,
                            GUID_Parent = localConfig.OItem_type_kommunikationsangaben.GUID,
                            Type = localConfig.Globals.Type_Object
                        };

                        transaction.ClearItems();

                        result = transaction.do_Transaction(oItemKommunikationsangaben);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var rel = relationConfig.Rel_ObjectRelation(oItemKommunikationsangaben, oItemPartner, localConfig.OItem_relationtype_belongsto);

                            result = transaction.do_Transaction(rel, boolRemoveAll: true);
                        }

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                            oItemKommunikationsangaben = null;
                        }

                    }
                }
            }

            OItemKommunikationsangaben = oItemKommunikationsangaben;
            ResultKommunikationsangabenSave = result;
            return result;
        }

        public async Task<clsOntologyItem> SaveNatuerlichePerson(clsOntologyItem oItemPartner)
        {
            var result = localConfig.Globals.LState_Success.Clone();
            clsOntologyItem oItemNatuerlichePerson = null;

            lock (serviceLocker)
            {
                var dbReader = new OntologyModDBConnector(localConfig.Globals);

                var searchNatuerlichePerson = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemPartner.GUID,
                        ID_RelationType = localConfig.OItem_relationtype_belongsto.GUID,
                        ID_Parent_Object = localConfig.OItem_type_nat_rliche_person.GUID
                    }
                };

                result = dbReader.GetDataObjectRel(searchNatuerlichePerson);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    oItemNatuerlichePerson = dbReader.ObjectRels.Select(addrRel => new clsOntologyItem
                    {
                        GUID = addrRel.ID_Object,
                        Name = addrRel.Name_Object,
                        GUID_Parent = addrRel.ID_Parent_Object,
                        Type = localConfig.Globals.Type_Object
                    }).FirstOrDefault();

                    if (oItemNatuerlichePerson == null)
                    {
                        oItemNatuerlichePerson = new clsOntologyItem
                        {
                            GUID = localConfig.Globals.NewGUID,
                            Name = oItemPartner.Name,
                            GUID_Parent = localConfig.OItem_type_nat_rliche_person.GUID,
                            Type = localConfig.Globals.Type_Object
                        };

                        transaction.ClearItems();

                        result = transaction.do_Transaction(oItemNatuerlichePerson);

                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var rel = relationConfig.Rel_ObjectRelation(oItemNatuerlichePerson, oItemPartner, localConfig.OItem_relationtype_belongsto);

                            result = transaction.do_Transaction(rel, boolRemoveAll: true);
                        }

                        if (result.GUID == localConfig.Globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                            oItemNatuerlichePerson = null;
                        }

                    }
                }
            }

            OItemNatuerlichePerson = oItemNatuerlichePerson;
            ResultNatuerlichePersonSave = result;
            return result;
        }

        public ServiceAgentElastic()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            
        }

        public ServiceAgentElastic(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }
        }

        public ServiceAgentElastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            transaction = new clsTransaction(localConfig.Globals);
            relationConfig = new clsRelationConfig(localConfig.Globals);
        }
    }

    public class SearchResult<TType>
    {
        public clsOntologyItem Result { get; set; }

        public List<TType> ResultList { get; set; }
        public List<TType> ResultListRightLeft { get; set; }
    }
    public class ServiceResult
    {
        private string idAttributeType_Strasse;
        private string idAttributeType_Postfach;
        private string idRelationType_tel;
        private string idRelationType_fax;


        public ServiceResult(string idAttributeType_Strasse, 
            string idAttributeType_Postfach, 
            string idRelationType_tel,
            string idRelationType_fax)
        {
            this.idAttributeType_Postfach = idAttributeType_Postfach;
            this.idAttributeType_Strasse = idAttributeType_Strasse;
            this.idRelationType_fax = idRelationType_fax;
            this.idRelationType_tel = idRelationType_tel;
        }

        public clsOntologyItem Result { get; set; }
        #region DataProps

        private List<clsObjectRel> partnersOfRefLeftRight;
        public List<clsObjectRel> PartnersOfRefLeftRight
        {
            get
            {
                return partnersOfRefLeftRight;
                

            }
            set
            {
                partnersOfRefLeftRight = value;
                
            }
        }

        private List<clsObjectRel> partnersOfRefRightLeft;
        public List<clsObjectRel> PartnersOfRefRightLeft
        {
            get
            {
                return partnersOfRefRightLeft;
                
            }
            set
            {
                partnersOfRefRightLeft = value;
                
            }
        }

        #region AdressProperties

        private List<clsObjectRel> partnersToAddresses;
        public List<clsObjectRel> PartnersToAddresses
        {
            get
            {
                return partnersToAddresses;
                

            }
            set
            {
                partnersToAddresses = value;
            }
        }

        private List<clsObjectAtt> addressAttributes;
        public List<clsObjectAtt> AddressAttributes
        {
            get
            {
                return addressAttributes;
                
            }
            set
            {
                addressAttributes = value;
                
            }
        }

        public List<clsObjectAtt> Strassen
        {
            get
            {
                return addressAttributes.Where(att => att.ID_AttributeType == idAttributeType_Strasse).ToList();
            }
        }

        public List<clsObjectAtt> Postfaecher
        {
            get
            {
                return addressAttributes.Where(att => att.ID_AttributeType == idAttributeType_Postfach).ToList();
            }
        }

        private List<clsObjectRel> plzs;
        public List<clsObjectRel> Plzs
        {
            get
            {
                return plzs;
                
            }
            set
            {
                plzs = value;
                
            }
        }

        private List<clsObjectRel> orte;
        public List<clsObjectRel> Orte
        {
            get
            {
                return orte;
                
            }
            set
            {
                orte = value;
                
            }
        }

        private List<clsObjectRel> lands;
        public List<clsObjectRel> Lands
        {
            get
            {
                return lands;
                
            }
            set
            {
                lands = value;
                
            }
        }

        #endregion

        #region CommunicationProperties

        private List<clsObjectRel> communicationToPartners;
        public List<clsObjectRel> CommunicationToPartners
        {
            get
            {
                return communicationToPartners;
                
            }
            set
            {
                communicationToPartners = value;
                
            }
        }

        private List<clsObjectRel> communicationToEmailAddresses;
        public List<clsObjectRel> CommunicationToEmailAddresses
        {
            get
            {
                return communicationToEmailAddresses;
                
            }
            set
            {
                communicationToEmailAddresses = value;
                
            }
        }

        private List<clsObjectRel> communicationToPhoneNumbers;
        public List<clsObjectRel> CommunicationToPhoneNumbers
        {
            get
            {
                return communicationToPhoneNumbers;
                
            }
            set
            {
                communicationToPhoneNumbers = value;
                
            }
        }

        public List<clsObjectRel> CommunicationToTels
        {
            get
            {
                return communicationToPhoneNumbers.Where(com => com.ID_RelationType == idRelationType_tel).ToList();
            }
        }

        public List<clsObjectRel> CommunicationToFaxs
        {
            get
            {
                return communicationToPhoneNumbers.Where(com => com.ID_RelationType == idRelationType_fax).ToList();
            }
        }

        private List<clsObjectRel> communicationToUrls;
        public List<clsObjectRel> CommunicationToUrls
        {
            get
            {
                return communicationToUrls;
                
            }
            set
            {
                communicationToUrls = value;
                
            }
        }

        #endregion

        #region PersonalDataProperties

        private List<clsObjectRel> personalDataToPartner;
        public List<clsObjectRel> PersonalDataToPartner
        {
            get
            {
                return personalDataToPartner;


            }
            set
            {
                personalDataToPartner = value;
            }
        }

        private List<clsObjectAtt> personalDataToGeburtsdatum;
        public List<clsObjectAtt> PersonalDataToGeburtsdatum
        {
            get
            {
                return personalDataToGeburtsdatum;


            }
            set
            {
                personalDataToGeburtsdatum = value;
            }
        }

        private List<clsObjectAtt> personalDataToNachname;
        public List<clsObjectAtt> PersonalDataToNachname
        {
            get
            {
                return personalDataToNachname;


            }
            set
            {
                personalDataToNachname = value;
            }
        }

        private List<clsObjectAtt> personalDataToTodesdatum;
        public List<clsObjectAtt> PersonalDataToTodesdatum
        {
            get
            {
                return personalDataToTodesdatum;


            }
            set
            {
                personalDataToTodesdatum = value;
            }
        }

        private List<clsObjectAtt> personalDataToVorname;
        public List<clsObjectAtt> PersonalDataToVorname
        {
            get
            {
                return personalDataToVorname;


            }
            set
            {
                personalDataToVorname = value;
            }
        }

        private List<clsObjectRel> personalDataToETin;
        public List<clsObjectRel> PersonalDataToETin
        {
            get
            {
                return personalDataToETin;

            }
            set
            {
                personalDataToETin = value;

            }
        }

        private List<clsObjectRel> personalDataToFamilienstand;
        public List<clsObjectRel> PersonalDataToFamilienstand
        {
            get
            {
                return personalDataToFamilienstand;

            }
            set
            {
                personalDataToFamilienstand = value;

            }
        }

        private List<clsObjectRel> personalDataToGeschlecht;
        public List<clsObjectRel> PersonalDataToGeschlecht
        {
            get
            {
                return personalDataToGeschlecht;

            }
            set
            {
                personalDataToGeschlecht = value;

            }
        }

        private List<clsObjectRel> personalDataToIdentifikationsnummer;
        public List<clsObjectRel> PersonalDataToIdentifikationsnummer
        {
            get
            {
                return personalDataToIdentifikationsnummer;

            }
            set
            {
                personalDataToIdentifikationsnummer = value;

            }
        }

        private List<clsObjectRel> personalDataToSozialversicherungsnummer;
        public List<clsObjectRel> PersonalDataToSozialversicherungsnummer
        {
            get
            {
                return personalDataToSozialversicherungsnummer;

            }
            set
            {
                personalDataToSozialversicherungsnummer = value;

            }
        }

        private List<clsObjectRel> personalDataToSteuernummer;
        public List<clsObjectRel> PersonalDataToSteuernummer
        {
            get
            {
                return personalDataToSteuernummer;

            }
            set
            {
                personalDataToSteuernummer = value;

            }
        }

        private List<clsObjectRel> personalDataToGeburtsort;
        public List<clsObjectRel> PersonalDataToGeburtsort
        {
            get
            {
                return personalDataToGeburtsort;

            }
            set
            {
                personalDataToGeburtsort = value;

            }
        }

        #endregion  

        #endregion
    }

    
}
