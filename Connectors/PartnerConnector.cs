﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using PartnerModule.Models;
using PartnerModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Connectors
{
    public class PartnerConnector
    {
        private clsLocalConfig localConfig;
        private ServiceAgentElastic serviceAgentElastic;

        public async Task<List<Address>> GetAddressFull(List<clsOntologyItem> addressesRaw)
        {
            var serviceResultTask = serviceAgentElastic.GetAddresses(addressesRaw);
            serviceResultTask.Wait();

            var serviceResult = serviceResultTask.Result;

            var addresses = (from address in addressesRaw
                             join strasse in serviceResult.Strassen on address.GUID equals strasse.ID_Object into strassen
                             from strasse in strassen.DefaultIfEmpty()
                             join postfach in serviceResult.Postfaecher on address.GUID equals postfach.ID_Object into postfaecher
                             from postfach in postfaecher.DefaultIfEmpty()
                             join plzItem in serviceResult.Plzs on address.GUID equals plzItem.ID_Object
                             join ortItem in serviceResult.Orte on address.GUID equals ortItem.ID_Object
                             join land in serviceResult.Lands on ortItem.ID_Other equals land.ID_Object
                             select new Address
                             {
                                 Id = address.GUID,
                                 Name = address.Name,
                                 IdStrasse = strasse != null ? strasse.ID_Attribute : null,
                                 NameStrasse = strasse != null ?  strasse.Val_String : null,
                                 IdPostfach = postfach != null ? postfach.ID_Attribute : null,
                                 NamePostfach = postfach != null ? postfach.Val_String : null,
                                 IdPlz = plzItem.ID_Other,
                                 Plz = plzItem.Name_Other,
                                 IdOrt = ortItem.ID_Other,
                                 NameOrt = ortItem.Name_Other,
                                 IdLand = land.ID_Other,
                                 NameLand = land.Name_Other
                             }).ToList();

            return addresses;
        }

        public PartnerConnector(Globals globals)
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(localConfig);
        }
    }
}
