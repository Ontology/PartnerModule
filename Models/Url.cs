﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class Url
    {
        public string IdUrl { get; set; }
        public string NameUrl { get; set; }
    }
}
