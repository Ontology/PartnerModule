﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class RefToPartner
    {
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        public string IdRelationType { get; set; }
        public string IdDirection { get; set; }
        public Partner Partner { get; set; }
    }
}
