﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class EmailAddress
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
