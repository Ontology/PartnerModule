﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Models
{
    public class Address
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string IdPostfach { get; set; }
        public string NamePostfach { get; set; }

        public string IdStrasse { get; set; }
        public string NameStrasse { get; set; }

        public string IdPlz { get; set; }
        public string Plz { get; set; }

        public string IdOrt { get; set; }
        public string NameOrt { get; set; }

        public string IdLand { get; set; }
        public string NameLand { get; set; }

       
    }
}
