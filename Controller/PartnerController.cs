﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Helpers;
using PartnerModule.Factories;
using PartnerModule.Services;
using PartnerModule.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace PartnerModule.Controller
{
    public class PartnerController: PartnerViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private clsLocalConfig localConfig;

        private clsOntologyItem oItemSended;

        private ServiceAgentElastic serviceAgentElastic;

        private InputTransactionHelper nameInputTransactionHelper;
        private InputTransactionHelper strasseInputTransactionHelper;
        private InputTransactionHelper postfachTransactionHelper;

        private InputTransactionHelper vornameTransactionHelper;
        private InputTransactionHelper nachnameTransactionHelper;

        private clsOntologyItem oItemAddress;
        private clsOntologyItem oItemPLZ;
        private clsOntologyItem oItemOrt;
        private clsOntologyItem oItemLand;

        private clsOntologyItem oItemKommunikationsAngaben;
        private clsOntologyItem oItemNatuerlichePerson;
        private clsOntologyItem oItemGeschlecht;
        private clsOntologyItem oItemFamilienstand;
        private clsOntologyItem oItemGeburtsort;
        private clsOntologyItem oItemSozialversicherungsnummer;
        private clsOntologyItem oItemETin;
        private clsOntologyItem oItemIdentifikationsnummer;
        private clsOntologyItem oItemSteuernummer;
        private clsOntologyItem oItemTelefon;
        private clsOntologyItem oItemFax;
        private clsOntologyItem oItemEmail;
        private clsOntologyItem oItemUrl;

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public PartnerController()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += PartnerController_PropertyChanged;
        }

        private void PartnerController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Notifications.NotifyChanges.ViewModel_OItem_Partner)
            {
                var partnerServiceTask = serviceAgentElastic.GetPartnersByRef(OItem_Partner);
                partnerServiceTask.Wait();
                var partnerServiceResult = partnerServiceTask.Result;

                if (partnerServiceResult.Result.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    return;
                }

                oItemAddress = partnerServiceResult.PartnersToAddresses.Select(addrRel => new clsOntologyItem
                {
                    GUID = addrRel.ID_Other,
                    Name = addrRel.Name_Other,
                    GUID_Parent = addrRel.ID_Parent_Other,
                    Type = localConfig.Globals.Type_Object
                }).FirstOrDefault();

                Text_Name = OItem_Partner.Name;
                Text_Strasse = partnerServiceResult.Strassen.FirstOrDefault()?.Val_String;
                Text_Postfach = partnerServiceResult.Postfaecher.FirstOrDefault()?.Val_String;
                Text_PLZ = partnerServiceResult.Plzs.FirstOrDefault()?.Name_Other;
                Text_Ort = partnerServiceResult.Orte.FirstOrDefault()?.Name_Other;
                Text_Land = partnerServiceResult.Lands.FirstOrDefault()?.Name_Other;

                DateTimeNull_Geburtsdatum = partnerServiceResult.PersonalDataToGeburtsdatum.FirstOrDefault()?.Val_Date;
                DataText_Nachname = partnerServiceResult.PersonalDataToNachname.FirstOrDefault()?.Val_String;
                DateTimeNull_Todesdatum = partnerServiceResult.PersonalDataToTodesdatum.FirstOrDefault()?.Val_Date;
                DataText_Vorname = partnerServiceResult.PersonalDataToVorname.FirstOrDefault()?.Val_String;

                DataText_eTin = partnerServiceResult.PersonalDataToETin.FirstOrDefault()?.Name_Other;
                DataText_Familienstand = partnerServiceResult.PersonalDataToFamilienstand.FirstOrDefault()?.Name_Other;
                DataText_Geschlecht = partnerServiceResult.PersonalDataToGeschlecht.FirstOrDefault()?.Name_Other;
                DataText_Identifkationsnummer = partnerServiceResult.PersonalDataToIdentifikationsnummer.FirstOrDefault()?.Name_Other;
                DataText_Sozialversicherungsnummer = partnerServiceResult.PersonalDataToSozialversicherungsnummer.FirstOrDefault()?.Name_Other;
                DataText_Steuernummer = partnerServiceResult.PersonalDataToSteuernummer.FirstOrDefault()?.Name_Other;
                DataText_Geburtsort = partnerServiceResult.PersonalDataToGeburtsort.FirstOrDefault()?.Name_Other;

                refreshCommunictionInfos(partnerServiceResult);

            }

            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);

            
        }

        private void refreshCommunictionInfos(ServiceResult partnerServiceResult)
        {
            var communicationItem = partnerServiceResult.CommunicationToPartners.FirstOrDefault();
            if (communicationItem != null)
            {


                var factory = new PartnerFactory(localConfig.Globals);
                var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");

                var resultTask = factory.CreateJsonCom(sessionFile, partnerServiceResult.CommunicationToPartners,
                    partnerServiceResult.CommunicationToEmailAddresses,
                    partnerServiceResult.CommunicationToPhoneNumbers,
                    partnerServiceResult.CommunicationToFaxs,
                    partnerServiceResult.CommunicationToUrls,
                    localConfig);

                resultTask.Wait();
                if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    JqxDataSource_Grid = new OntoWebCore.Models.JqxDataSource(OntoWebCore.Models.JsonDataType.Json)
                    {
                        pagesize = 20,
                        url = sessionFile.FileUri.AbsoluteUri
                    };

                }


            }
        }
        
        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            


        }

        private void StateMachine_closedSocket()
        {
            
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            Text_View = translationController.Text_View;

            //if (webSocketServiceAgent.Request.ContainsKey("ListType"))
            //{

            //}
        }

        private void StateMachine_loginSucceded()
        {

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });

            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.AppliedObjects,
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Receiver,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });


            ToggleEnableState(false);


            
            var sessionFile = webSocketServiceAgent.RequestWriteStream(Guid.NewGuid().ToString() + ".json");


            using (sessionFile.StreamWriter)
            {
                using (var jsonWriter = new Newtonsoft.Json.JsonTextWriter(sessionFile.StreamWriter))
                {
                    jsonWriter.WriteStartArray();
                    jsonWriter.WriteEndArray();
                }
            }

            JqxDataSource_Grid = new OntoWebCore.Models.JqxDataSource(OntoWebCore.Models.JsonDataType.Json)
            {
                pagesize = 20,
                url = sessionFile.FileUri.AbsoluteUri
            };

            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(LocalStateMachine.LoginSuccessful))
            {
                IsListen_Land = IsListen_Ort = IsListen_Plz = false;
                
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
                
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;




        }

        

        private void ToggleEnableState(bool enable)
        {
            IsEnabled_Geburtsdatum = enable;
            IsEnabled_Nachname = enable;
            IsEnabled_Name = enable;
            IsEnabled_Postfach = enable;
            IsEnabled_Strasse = enable;
            IsEnabled_Todesdatum = enable;
            IsEnabled_Vorname = enable;
        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenPlz"))
                {
                    IsListen_Plz = !IsListen_Plz;
                    if (IsListen_Plz)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_postleitzahl);
                    }
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenOrt"))
                {
                    IsListen_Ort = !IsListen_Ort;
                    if (IsListen_Ort)
                    {
                        IsListen_Geburtsort = false;
                    }

                    if (IsListen_Ort)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_ort);
                    }
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenLand"))
                {
                    IsListen_Land = !IsListen_Land;
                    if (IsListen_Land)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_land);
                    }
                    
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenGeschlecht"))
                {
                    IsListen_Geschlecht = !IsListen_Geschlecht;
                    if (IsListen_Geschlecht)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_geschlecht);
                    }

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenFamilienstand"))
                {
                    IsListen_Familienstand = !IsListen_Familienstand;
                    if (IsListen_Familienstand)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_familienstand);
                    }

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenGebursort"))
                {
                    IsListen_Geburtsort = !IsListen_Geburtsort;
                    if (IsListen_Geburtsort)
                    {
                        IsListen_Ort = false;
                    }

                    if (IsListen_Geburtsort)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_ort);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenSozialsversicherungsnummer"))
                {
                    IsListen_Sozialversicherungsnummer = !IsListen_Sozialversicherungsnummer;
                    if (IsListen_Sozialversicherungsnummer)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_sozialversicherungsnummer);
                    }

                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenETin"))
                {
                    IsListen_eTin = !IsListen_eTin;
                    if (IsListen_eTin)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_etin);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenIndentifikationsNr"))
                {
                    IsListen_Identifkationsnummer = !IsListen_Identifkationsnummer;
                    if (IsListen_Identifkationsnummer)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_identifkationsnummer__idnr_);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenSteuernummer"))
                {
                    IsListen_Steuernummer = !IsListen_Steuernummer;
                    if (IsListen_Steuernummer)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_steuernummer);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenTelephone"))
                {
                    IsListen_ListenTelephone = !IsListen_ListenTelephone;
                    if (IsListen_ListenTelephone)
                    {
                        IsListen_ListenFax = false;
                    }

                    if (IsListen_ListenTelephone)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_telefonnummer);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenFax"))
                {
                    IsListen_ListenFax = !IsListen_ListenFax;
                    if (IsListen_ListenFax)
                    {
                        IsListen_ListenTelephone = false;
                    }

                    if (IsListen_ListenFax)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_telefonnummer);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenEmail"))
                {
                    IsListen_ListenEmail = !IsListen_ListenEmail;
                    if (IsListen_ListenEmail)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_email_address);
                    }
                }
                else if (webSocketServiceAgent.Command_RequestedCommand.StartsWith("clicked.isListenUrl"))
                {
                    IsListen_ListenUrl = !IsListen_ListenUrl;
                    if (IsListen_ListenUrl)
                    {
                        SendClassSelectMessage(localConfig.OItem_type_url);
                    }
                }
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedProperty)
            {

            }
            else if (e.PropertyName == nameof(WebsocketServiceAgent.ChangedViewItems))
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedViewItem =>
                {
                    if (changedViewItem.ViewItemId == "inpNameSave" && changedViewItem.ViewItemType == ViewItemType.Content.ToString())
                    {
                        GetAddress();

                        if (oItemAddress != null)
                        {
                            nameInputTransactionHelper.Value = changedViewItem.ViewItemValue.ToString();
                        }
                        
                    }
                    else if (changedViewItem.ViewItemId == "inpStrasseSave" && changedViewItem.ViewItemType == ViewItemType.Content.ToString())
                    {
                        GetAddress();

                        if (oItemAddress != null)
                        {
                            strasseInputTransactionHelper.Value = changedViewItem.ViewItemValue.ToString();
                        }
                            
                    }
                    else if (changedViewItem.ViewItemId == "inpPostfachSave" && changedViewItem.ViewItemType == ViewItemType.Content.ToString())
                    {
                        GetAddress();
                        
                        if (oItemAddress != null)
                        {
                            postfachTransactionHelper.Value = changedViewItem.ViewItemValue.ToString();
                        }
                            
                    }
                    else if (changedViewItem.ViewItemId == "inpVornameSave" && changedViewItem.ViewItemType == ViewItemType.Content.ToString())
                    {
                        GetNatuerlichePerson();
                        if (oItemNatuerlichePerson != null)
                        {
                            vornameTransactionHelper.Value = changedViewItem.ViewItemValue.ToString();
                        }

                    }
                    else if (changedViewItem.ViewItemId == "inpNachnameSave" && changedViewItem.ViewItemType == ViewItemType.Content.ToString())
                    {
                        GetNatuerlichePerson();
                        if (oItemNatuerlichePerson != null)
                        {
                            nachnameTransactionHelper.Value = changedViewItem.ViewItemValue.ToString();
                        }

                    }
                });
            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument && webSocketServiceAgent.ObjectArgument != null)
            {
                if (localConfig.Globals.is_GUID(webSocketServiceAgent.ObjectArgument.Value.ToString()))
                {
                    CheckSendOrParamObject(webSocketServiceAgent.ObjectArgument.Value.ToString());
                }
                
            }


        }

        private void GetNatuerlichePerson()
        {
            if (OItem_Partner == null)
            {
                oItemNatuerlichePerson = null;
                return;
            }

            if (oItemNatuerlichePerson != null)
            {
                SetNatuerlichePersonTransactionHelpers();
                return;
            }
            var resultTask = serviceAgentElastic.SaveNatuerlichePerson(OItem_Partner);
            resultTask.Wait();

            oItemNatuerlichePerson = serviceAgentElastic.OItemNatuerlichePerson;
            if (oItemNatuerlichePerson != null)
            {
                SetNatuerlichePersonTransactionHelpers();
            }
            else
            {
                vornameTransactionHelper = null;
                nachnameTransactionHelper = null;
            }
        }

        private void GetKommunikationsAngaben()
        {
            if (OItem_Partner == null)
            {
                oItemKommunikationsAngaben = null;
                return;
            }

            if (oItemKommunikationsAngaben != null)
            {
                SetKommunikationsTransactionHelpers();
                return;
            }
            var resultTask = serviceAgentElastic.SaveKommunikationsangaben(OItem_Partner);
            resultTask.Wait();

            oItemKommunikationsAngaben = serviceAgentElastic.OItemKommunikationsangaben;
            if (oItemKommunikationsAngaben != null)
            {
                SetKommunikationsTransactionHelpers();
            }
            else
            {
            
            }
        }

        private void GetAddress()
        {
            if (OItem_Partner == null)
            {
                oItemAddress = null;
                return;
            }

            if (oItemAddress != null)
            {
                SetAddressTransactionHelpers();
                return;
            }
            var resultTask = serviceAgentElastic.SaveAddress(OItem_Partner);
            resultTask.Wait();

            oItemAddress = serviceAgentElastic.OItemAddress;
            if (oItemAddress != null)
            {
                SetAddressTransactionHelpers();
            }
            else
            {
                nameInputTransactionHelper = null;
                strasseInputTransactionHelper = null;
                postfachTransactionHelper = null;
            }
            
        }

        private void SetKommunikationsTransactionHelpers()
        {
            
        }

        private void SetNatuerlichePersonTransactionHelpers()
        {
            vornameTransactionHelper = new InputTransactionHelper(localConfig.Globals, oItemNatuerlichePerson, localConfig.OItem_attribute_vorname, false, true, true);
            vornameTransactionHelper.savedInput += VornameTransactionHelper_savedInput;

            nachnameTransactionHelper = new InputTransactionHelper(localConfig.Globals, oItemNatuerlichePerson, localConfig.OItem_attribute_nachname, false, true, true);
            nachnameTransactionHelper.savedInput += NachnameTransactionHelper_savedInput;
        }

        private void NachnameTransactionHelper_savedInput(clsOntologyItem result)
        {
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RaisePropertyChanged(nameof(DataText_Nachname));
                IsEnabled_Nachname = false;

            }
        }

        private void VornameTransactionHelper_savedInput(clsOntologyItem result)
        {
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RaisePropertyChanged(nameof(DataText_Vorname));
                IsEnabled_Vorname = false;

            }
        }

        private void SetAddressTransactionHelpers()
        {
            nameInputTransactionHelper = new InputTransactionHelper(localConfig.Globals, OItem_Partner, true);
            nameInputTransactionHelper.savedInput += NameInputTransactionHelper_savedInput;

            strasseInputTransactionHelper = new InputTransactionHelper(localConfig.Globals, oItemAddress, localConfig.OItem_attribute_straße, false, true, true);
            strasseInputTransactionHelper.savedInput += StrasseInputTransactionHelper_savedInput;

            postfachTransactionHelper = new InputTransactionHelper(localConfig.Globals, oItemAddress, localConfig.OItem_attribute_postfach, false, true, true);
            postfachTransactionHelper.savedInput += PostfachTransactionHelper_savedInput;
        }

        private void CheckSendOrParamObject(string idItem)
        {
            var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

            if (paramObject.GUID_Parent == localConfig.OItem_type_partner.GUID)
            {
                OItem_Partner = paramObject;

                

                Text_View = OItem_Partner.Name;

                ToggleEnableState(true);
            }
            
        }

        private void PostfachTransactionHelper_savedInput(clsOntologyItem result)
        {
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RaisePropertyChanged(nameof(Text_Postfach));
                IsEnabled_Postfach = false;

            }
        }

        private void StrasseInputTransactionHelper_savedInput(clsOntologyItem result)
        {
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RaisePropertyChanged(nameof(Text_Strasse));
                IsEnabled_Strasse = false;
            }
        }

        private void NameInputTransactionHelper_savedInput(clsOntologyItem result)
        {
            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                RaisePropertyChanged(nameof(Text_Name));
                IsEnabled_Name = false;
            }
        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {


            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;
            if (message.ChannelId == Channels.ParameterList)
            {
                ToggleEnableState(false);

                var oItemMessage = message.OItems.LastOrDefault();
                if (oItemMessage == null) return;
                if ((oItemSended != null && oItemMessage.GUID != oItemSended.GUID) || oItemSended == null)
                {
                    oItemSended = oItemMessage;
                    CheckSendOrParamObject(oItemSended.GUID);


                }



            }
            else if (message.ChannelId == Channels.AppliedObjects)
            {
                var paramObject = message.OItems.FirstOrDefault();

                if (paramObject == null) return;
                if (OItem_Partner == null) return;

                if (paramObject.GUID_Parent == localConfig.OItem_type_postleitzahl.GUID && IsListen_Plz)
                {
                    oItemPLZ = paramObject;

                    GetAddress();

                    if (oItemAddress != null)
                    {
                        var resultTask = serviceAgentElastic.SavePLZOrOrt(oItemAddress, oItemPLZ);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            Text_PLZ = serviceAgentElastic.OItemPlz != null ? serviceAgentElastic.OItemPlz.Name : "";
                            Text_Ort = serviceAgentElastic.OItemOrt != null ? serviceAgentElastic.OItemOrt.Name : "";
                            oItemLand = serviceAgentElastic.OItemLand != null ? serviceAgentElastic.OItemLand : null;
                            Text_Land = serviceAgentElastic.OItemLand != null ? serviceAgentElastic.OItemLand.Name : "";
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_ort.GUID && IsListen_Ort)
                {
                    oItemOrt = paramObject;

                    GetAddress();

                    if (oItemAddress != null)
                    {
                        var resultTask = serviceAgentElastic.SavePLZOrOrt(oItemAddress, oItemOrt);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            Text_PLZ = serviceAgentElastic.OItemPlz != null ? serviceAgentElastic.OItemPlz.Name : "";
                            Text_Ort = serviceAgentElastic.OItemOrt != null ? serviceAgentElastic.OItemOrt.Name : "";
                            oItemLand = serviceAgentElastic.OItemLand != null ? serviceAgentElastic.OItemLand : null;
                            Text_Land = serviceAgentElastic.OItemLand != null ? serviceAgentElastic.OItemLand.Name : "";
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_ort.GUID && IsListen_Geburtsort)
                {
                    oItemGeburtsort = paramObject;

                    GetNatuerlichePerson();

                    if (oItemGeburtsort != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemGeburtsort );
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_Geburtsort = oItemGeburtsort.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_geschlecht.GUID && IsListen_Geschlecht)
                {
                    oItemGeschlecht = paramObject;

                    GetNatuerlichePerson();

                    if (oItemNatuerlichePerson != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemGeschlecht);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_Geschlecht = oItemGeschlecht.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_familienstand.GUID && IsListen_Familienstand)
                {
                    oItemFamilienstand = paramObject;

                    GetNatuerlichePerson();

                    if (oItemNatuerlichePerson != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemFamilienstand);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_Familienstand = oItemFamilienstand.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_sozialversicherungsnummer.GUID && IsListen_Sozialversicherungsnummer)
                {
                    oItemSozialversicherungsnummer = paramObject;

                    GetNatuerlichePerson();

                    if (oItemNatuerlichePerson != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemSozialversicherungsnummer);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_Sozialversicherungsnummer = oItemSozialversicherungsnummer.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_etin.GUID && IsListen_eTin)
                {
                    oItemETin = paramObject;

                    GetNatuerlichePerson();

                    if (oItemNatuerlichePerson != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemETin);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_eTin = oItemETin.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_identifkationsnummer__idnr_.GUID && IsListen_Identifkationsnummer)
                {
                    oItemIdentifikationsnummer = paramObject;

                    GetNatuerlichePerson();

                    if (oItemNatuerlichePerson != null)
                    {
                        var resultTask = serviceAgentElastic.SaveNatuerlichPersonData(oItemNatuerlichePerson, oItemIdentifikationsnummer);
                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            DataText_Identifkationsnummer = oItemIdentifikationsnummer.Name;
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_telefonnummer.GUID && IsListen_ListenTelephone)
                {
                    oItemTelefon = paramObject;

                    GetKommunikationsAngaben();

                    if (oItemKommunikationsAngaben != null)
                    {
                        var resultTask = serviceAgentElastic.SaveKommunikationsAngabenData(oItemKommunikationsAngaben, oItemTelefon, localConfig.OItem_relationtype_tel );

                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var factory = new PartnerFactory(localConfig.Globals);

                            var comResultTask = factory.GetNewComItem(serviceAgentElastic.KomItemRel, localConfig);

                            comResultTask.Wait();

                            if (comResultTask.Result != null)
                            {
                                Dictionary_NewKomItem = comResultTask.Result;
                            }
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_telefonnummer.GUID && IsListen_ListenFax)
                {
                    oItemFax = paramObject;

                    GetKommunikationsAngaben();

                    if (oItemKommunikationsAngaben != null)
                    {
                        var resultTask = serviceAgentElastic.SaveKommunikationsAngabenData(oItemKommunikationsAngaben, oItemFax, localConfig.OItem_relationtype_fax);

                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var factory = new PartnerFactory(localConfig.Globals);

                            var comResultTask = factory.GetNewComItem(serviceAgentElastic.KomItemRel, localConfig);

                            comResultTask.Wait();

                            if (comResultTask.Result != null)
                            {
                                Dictionary_NewKomItem = comResultTask.Result;
                            }
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_email_address.GUID && IsListen_ListenEmail)
                {
                    oItemEmail = paramObject;

                    GetKommunikationsAngaben();

                    if (oItemKommunikationsAngaben != null)
                    {
                        var resultTask = serviceAgentElastic.SaveKommunikationsAngabenData(oItemKommunikationsAngaben, oItemEmail, localConfig.OItem_relationtype_contains);

                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var factory = new PartnerFactory(localConfig.Globals);

                            var comResultTask = factory.GetNewComItem(serviceAgentElastic.KomItemRel, localConfig);

                            comResultTask.Wait();

                            if (comResultTask.Result != null)
                            {
                                Dictionary_NewKomItem = comResultTask.Result;
                            }
                        }
                    }
                }
                else if (paramObject.GUID_Parent == localConfig.OItem_type_url.GUID && IsListen_ListenUrl)
                {
                    oItemUrl = paramObject;

                    GetKommunikationsAngaben();

                    if (oItemKommunikationsAngaben != null)
                    {
                        var resultTask = serviceAgentElastic.SaveKommunikationsAngabenData(oItemKommunikationsAngaben, oItemUrl, localConfig.OItem_relationtype_contains);

                        resultTask.Wait();

                        if (resultTask.Result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var factory = new PartnerFactory(localConfig.Globals);

                            var comResultTask = factory.GetNewComItem(serviceAgentElastic.KomItemRel, localConfig);

                            comResultTask.Wait();

                            if (comResultTask.Result != null)
                            {
                                Dictionary_NewKomItem = comResultTask.Result;
                            }
                        }
                    }
                }
            }
            


        }

        private void CheckPLZOrtLand(string idItem)
        {
            if (!localConfig.Globals.is_GUID(idItem) && OItem_Partner == null) return;

            var oItem = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);

            if (oItem == null) return;

            if (oItem.GUID == localConfig.OItem_type_ort.GUID)
            {
                
            }
            else if (oItem.GUID == localConfig.OItem_type_postleitzahl.GUID)
            {

            }
            else if (oItem.GUID == localConfig.OItem_type_land.GUID)
            {

            }

        }


        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }

        private void SendClassSelectMessage(clsOntologyItem classItem)
        {
            var interServiceMessage = new InterServiceMessage
            {
                ChannelId = Channels.SelectedClassNode,
                OItems = new List<clsOntologyItem>
                {
                    classItem
                }
            };

            webSocketServiceAgent.SendInterModMessage(interServiceMessage);
        }
    }
}
