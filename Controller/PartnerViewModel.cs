﻿using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoWebCore.Models;
using PartnerModule.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartnerModule.Controller
{
    public class PartnerViewModel : ViewModelBase
    {
        private bool issuccessful_Login;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "loginSuccess", ViewItemType = ViewItemType.Other)]
		public bool IsSuccessful_Login
        {
            get { return issuccessful_Login; }
            set
            {
                if (issuccessful_Login == value) return;

                issuccessful_Login = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsSuccessful_Login);

            }
        }

        private bool istoggled_Listen;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListen", ViewItemType = ViewItemType.Other)]
		public bool IsToggled_Listen
        {
            get { return istoggled_Listen; }
            set
            {
                if (istoggled_Listen == value) return;

                istoggled_Listen = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsToggled_Listen);

            }
        }

        private string text_View;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "viewText", ViewItemType = ViewItemType.Other)]
		public string Text_View
        {
            get { return text_View; }
            set
            {
                if (text_View == value) return;

                text_View = value;

                RaisePropertyChanged(NotifyChanges.View_Text_View);

            }
        }

        private string text_Land;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpLand", ViewItemType = ViewItemType.Content)]
		public string Text_Land
        {
            get { return text_Land; }
            set
            {
                if (text_Land == value) return;

                text_Land = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Land);

            }
        }

        private string label_Land;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblLand", ViewItemType = ViewItemType.Content)]
		public string Label_Land
        {
            get { return label_Land; }
            set
            {
                if (label_Land == value) return;

                label_Land = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Land);

            }
        }

        private string text_Ort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpOrt", ViewItemType = ViewItemType.Content)]
		public string Text_Ort
        {
            get { return text_Ort; }
            set
            {
                if (text_Ort == value) return;

                text_Ort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Ort);

            }
        }

        private string label_Ort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblOrt", ViewItemType = ViewItemType.Content)]
		public string Label_Ort
        {
            get { return label_Ort; }
            set
            {
                if (label_Ort == value) return;

                label_Ort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Ort);

            }
        }

        private string text_PLZ;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPLZ", ViewItemType = ViewItemType.Content)]
		public string Text_PLZ
        {
            get { return text_PLZ; }
            set
            {
                if (text_PLZ == value) return;

                text_PLZ = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_PLZ);

            }
        }

        private string label_PLZ;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPLZ", ViewItemType = ViewItemType.Content)]
		public string Label_PLZ
        {
            get { return label_PLZ; }
            set
            {
                if (label_PLZ == value) return;

                label_PLZ = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_PLZ);

            }
        }

        private string text_Postfach;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPostfach", ViewItemType = ViewItemType.Content)]
		public string Text_Postfach
        {
            get { return text_Postfach; }
            set
            {
                if (text_Postfach == value) return;

                text_Postfach = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Postfach);

            }
        }

        private bool isenabled_Postfach;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpPostfach", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Postfach
        {
            get { return isenabled_Postfach; }
            set
            {

                isenabled_Postfach = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Postfach);

            }
        }

        private string label_Postfach;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblPostfach", ViewItemType = ViewItemType.Content)]
		public string Label_Postfach
        {
            get { return label_Postfach; }
            set
            {
                if (label_Postfach == value) return;

                label_Postfach = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Postfach);

            }
        }

        private string text_Strasse;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpStrasse", ViewItemType = ViewItemType.Content)]
		public string Text_Strasse
        {
            get { return text_Strasse; }
            set
            {
                if (text_Strasse == value) return;

                text_Strasse = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Text_Strasse);

            }
        }

        private bool isenabled_Strasse;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpStrasse", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Strasse
        {
            get { return isenabled_Strasse; }
            set
            {

                isenabled_Strasse = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Strasse);

            }
        }

        private string label_Strasse;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblStrasse", ViewItemType = ViewItemType.Content)]
		public string Label_Strasse
        {
            get { return label_Strasse; }
            set
            {
                if (label_Strasse == value) return;

                label_Strasse = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Strasse);

            }
        }

        private string text_Name;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpName", ViewItemType = ViewItemType.Content)]
		public string Text_Name
        {
            get { return text_Name; }
            set
            {
                if (text_Name == value) return;

                text_Name = value;

                RaisePropertyChanged(NotifyChanges.DataModel_Text_Name);

            }
        }

        private bool isenabled_Name;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpName", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Name
        {
            get { return isenabled_Name; }
            set
            {

                isenabled_Name = value;

                RaisePropertyChanged(NotifyChanges.DataModel_IsEnabled_Name);

            }
        }

        private string label_Name;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblName", ViewItemType = ViewItemType.Content)]
		public string Label_Name
        {
            get { return label_Name; }
            set
            {
                if (label_Name == value) return;

                label_Name = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Name);

            }
        }

        private clsOntologyItem oitem_Partner;
		public clsOntologyItem OItem_Partner
        {
            get { return oitem_Partner; }
            set
            {
                if (oitem_Partner == value) return;

                oitem_Partner = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_OItem_Partner);

            }
        }

        private bool islisten_Land;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListenLand", ViewItemType = ViewItemType.Other)]
		public bool IsListen_Land
        {
            get { return islisten_Land; }
            set
            {

                islisten_Land = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Land);

            }
        }

        private bool islisten_Ort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListenOrt", ViewItemType = ViewItemType.Other)]
		public bool IsListen_Ort
        {
            get { return islisten_Ort; }
            set
            {

                islisten_Ort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Ort);

            }
        }

        private bool islisten_Plz;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Other, ViewItemId = "isListenPlz", ViewItemType = ViewItemType.Other)]
		public bool IsListen_Plz
        {
            get { return islisten_Plz; }
            set
            {

                islisten_Plz = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Plz);

            }
        }

        private string datatext_Steuernummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpSteuernummer", ViewItemType = ViewItemType.Content)]
		public string DataText_Steuernummer
        {
            get { return datatext_Steuernummer; }
            set
            {
                if (datatext_Steuernummer == value) return;

                datatext_Steuernummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Steuernummer);

            }
        }

        private bool islisten_Steuernummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenSteuernummer", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Steuernummer
        {
            get { return islisten_Steuernummer; }
            set
            {
                if (islisten_Steuernummer == value) return;

                islisten_Steuernummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Steuernummer);

            }
        }

        private string label_Steuernummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblSteuernummer", ViewItemType = ViewItemType.Content)]
		public string Label_Steuernummer
        {
            get { return label_Steuernummer; }
            set
            {
                if (label_Steuernummer == value) return;

                label_Steuernummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Steuernummer);

            }
        }

        private string datatext_Identifkationsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpIdentifkationsnummer", ViewItemType = ViewItemType.Content)]
		public string DataText_Identifkationsnummer
        {
            get { return datatext_Identifkationsnummer; }
            set
            {
                if (datatext_Identifkationsnummer == value) return;

                datatext_Identifkationsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Identifkationsnummer);

            }
        }

        private bool islisten_Identifkationsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenIndentifikationsNr", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Identifkationsnummer
        {
            get { return islisten_Identifkationsnummer; }
            set
            {
                if (islisten_Identifkationsnummer == value) return;

                islisten_Identifkationsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Identifkationsnummer);

            }
        }

        private string label_Identifkationsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblIdentifkationsnummer", ViewItemType = ViewItemType.Content)]
		public string Label_Identifkationsnummer
        {
            get { return label_Identifkationsnummer; }
            set
            {
                if (label_Identifkationsnummer == value) return;

                label_Identifkationsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Identifkationsnummer);

            }
        }

        private string datatext_eTin;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpETin", ViewItemType = ViewItemType.Content)]
		public string DataText_eTin
        {
            get { return datatext_eTin; }
            set
            {
                if (datatext_eTin == value) return;

                datatext_eTin = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_eTin);

            }
        }

        private bool islisten_eTin;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenETin", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_eTin
        {
            get { return islisten_eTin; }
            set
            {
                if (islisten_eTin == value) return;

                islisten_eTin = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_eTin);

            }
        }

        private string label_eTin;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblETin", ViewItemType = ViewItemType.Content)]
		public string Label_eTin
        {
            get { return label_eTin; }
            set
            {
                if (label_eTin == value) return;

                label_eTin = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_eTin);

            }
        }

        private string datatext_Sozialversicherungsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpSozialsversicherungsnummer", ViewItemType = ViewItemType.Content)]
		public string DataText_Sozialversicherungsnummer
        {
            get { return datatext_Sozialversicherungsnummer; }
            set
            {
                if (datatext_Sozialversicherungsnummer == value) return;

                datatext_Sozialversicherungsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Sozialversicherungsnummer);

            }
        }

        private bool islisten_Sozialversicherungsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenSozialsversicherungsnummer", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Sozialversicherungsnummer
        {
            get { return islisten_Sozialversicherungsnummer; }
            set
            {
                if (islisten_Sozialversicherungsnummer == value) return;

                islisten_Sozialversicherungsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Sozialversicherungsnummer);

            }
        }

        private string label_Sozialversicherungsnummer;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblSozialversicherungsnummer", ViewItemType = ViewItemType.Content)]
		public string Label_Sozialversicherungsnummer
        {
            get { return label_Sozialversicherungsnummer; }
            set
            {
                if (label_Sozialversicherungsnummer == value) return;

                label_Sozialversicherungsnummer = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Sozialversicherungsnummer);

            }
        }

        private string datatext_Geburtsort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpGebursort", ViewItemType = ViewItemType.Content)]
		public string DataText_Geburtsort
        {
            get { return datatext_Geburtsort; }
            set
            {
                if (datatext_Geburtsort == value) return;

                datatext_Geburtsort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Geburtsort);

            }
        }

        private bool islisten_Geburtsort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenGebursort", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Geburtsort
        {
            get { return islisten_Geburtsort; }
            set
            {
                if (islisten_Geburtsort == value) return;

                islisten_Geburtsort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Geburtsort);

            }
        }

        private string label_Geburtsort;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblGeburtsort", ViewItemType = ViewItemType.Content)]
		public string Label_Geburtsort
        {
            get { return label_Geburtsort; }
            set
            {
                if (label_Geburtsort == value) return;

                label_Geburtsort = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Geburtsort);

            }
        }

        private DateTime? datetimenull_Todesdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DatePicker, ViewItemId = "datepTodesdatum", ViewItemType = ViewItemType.Content)]
		public DateTime? DateTimeNull_Todesdatum
        {
            get { return datetimenull_Todesdatum; }
            set
            {
                if (datetimenull_Todesdatum == value) return;

                datetimenull_Todesdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DateTimeNull_Todesdatum);

            }
        }

        private bool isenabled_Todesdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DatePicker, ViewItemId = "datepTodesdatum", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Todesdatum
        {
            get { return isenabled_Todesdatum; }
            set
            {

                isenabled_Todesdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Todesdatum);

            }
        }

        private string label_Todesdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblTodesdatum", ViewItemType = ViewItemType.Content)]
		public string Label_Todesdatum
        {
            get { return label_Todesdatum; }
            set
            {
                if (label_Todesdatum == value) return;

                label_Todesdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Todesdatum);

            }
        }

        private DateTime? datetimenull_Geburtsdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DatePicker, ViewItemId = "datepGeburtsdatum", ViewItemType = ViewItemType.Content)]
		public DateTime? DateTimeNull_Geburtsdatum
        {
            get { return datetimenull_Geburtsdatum; }
            set
            {
                if (datetimenull_Geburtsdatum == value) return;

                datetimenull_Geburtsdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DateTimeNull_Geburtsdatum);

            }
        }

        private bool isenabled_Geburtsdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.DatePicker, ViewItemId = "datepGeburtsdatum", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Geburtsdatum
        {
            get { return isenabled_Geburtsdatum; }
            set
            {

                isenabled_Geburtsdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Geburtsdatum);

            }
        }

        private string label_Geburtsdatum;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblGeburtsdatum", ViewItemType = ViewItemType.Content)]
		public string Label_Geburtsdatum
        {
            get { return label_Geburtsdatum; }
            set
            {
                if (label_Geburtsdatum == value) return;

                label_Geburtsdatum = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Geburtsdatum);

            }
        }

        private string datatext_Familienstand;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpFamilienstand", ViewItemType = ViewItemType.Content)]
		public string DataText_Familienstand
        {
            get { return datatext_Familienstand; }
            set
            {
                if (datatext_Familienstand == value) return;

                datatext_Familienstand = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Familienstand);

            }
        }

        private bool islisten_Familienstand;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenFamilienstand", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Familienstand
        {
            get { return islisten_Familienstand; }
            set
            {
                if (islisten_Familienstand == value) return;

                islisten_Familienstand = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Familienstand);

            }
        }

        private string label_Familienstand;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblFamilienstand", ViewItemType = ViewItemType.Content)]
		public string Label_Familienstand
        {
            get { return label_Familienstand; }
            set
            {
                if (label_Familienstand == value) return;

                label_Familienstand = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Familienstand);

            }
        }

        private string datatext_Geschlecht;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpGeschlecht", ViewItemType = ViewItemType.Content)]
		public string DataText_Geschlecht
        {
            get { return datatext_Geschlecht; }
            set
            {
                if (datatext_Geschlecht == value) return;

                datatext_Geschlecht = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Geschlecht);

            }
        }

        private bool islisten_Geschlecht;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenGeschlecht", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_Geschlecht
        {
            get { return islisten_Geschlecht; }
            set
            {
                if (islisten_Geschlecht == value) return;

                islisten_Geschlecht = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_Geschlecht);

            }
        }

        private string label_Geschlecht;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblGeschlecht", ViewItemType = ViewItemType.Content)]
		public string Label_Geschlecht
        {
            get { return label_Geschlecht; }
            set
            {
                if (label_Geschlecht == value) return;

                label_Geschlecht = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Geschlecht);

            }
        }

        private string datatext_Nachname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpNachname", ViewItemType = ViewItemType.Content)]
		public string DataText_Nachname
        {
            get { return datatext_Nachname; }
            set
            {
                if (datatext_Nachname == value) return;

                datatext_Nachname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Nachname);

            }
        }

        private bool isenabled_Nachname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpNachname", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Nachname
        {
            get { return isenabled_Nachname; }
            set
            {

                isenabled_Nachname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Nachname);

            }
        }

        private string label_Nachname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblNachname", ViewItemType = ViewItemType.Content)]
		public string Label_Nachname
        {
            get { return label_Nachname; }
            set
            {
                if (label_Nachname == value) return;

                label_Nachname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Nachname);

            }
        }

        private string datatext_Vorname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVorname", ViewItemType = ViewItemType.Content)]
		public string DataText_Vorname
        {
            get { return datatext_Vorname; }
            set
            {
                if (datatext_Vorname == value) return;

                datatext_Vorname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_DataText_Vorname);

            }
        }

        private bool isenabled_Vorname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Input, ViewItemId = "inpVorname", ViewItemType = ViewItemType.Enable)]
		public bool IsEnabled_Vorname
        {
            get { return isenabled_Vorname; }
            set
            {

                isenabled_Vorname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsEnabled_Vorname);

            }
        }

        private string label_Vorname;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Label, ViewItemId = "lblVorname", ViewItemType = ViewItemType.Content)]
		public string Label_Vorname
        {
            get { return label_Vorname; }
            set
            {
                if (label_Vorname == value) return;

                label_Vorname = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Label_Vorname);

            }
        }

        private JqxDataSource jqxdatasource_Grid;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "grid", ViewItemType = ViewItemType.DataSource)]
		public JqxDataSource JqxDataSource_Grid
        {
            get { return jqxdatasource_Grid; }
            set
            {
                if (jqxdatasource_Grid == value) return;

                jqxdatasource_Grid = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_JqxDataSource_Grid);

            }
        }

        private bool islisten_ListenTelephone;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenTelephone", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_ListenTelephone
        {
            get { return islisten_ListenTelephone; }
            set
            {

                islisten_ListenTelephone = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_ListenTelephone);

            }
        }

        private bool islisten_ListenUrl;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenUrl", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_ListenUrl
        {
            get { return islisten_ListenUrl; }
            set
            {
                if (islisten_ListenUrl == value) return;

                islisten_ListenUrl = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_ListenUrl);

            }
        }

        private bool islisten_ListenEmail;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenEmail", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_ListenEmail
        {
            get { return islisten_ListenEmail; }
            set
            {
                if (islisten_ListenEmail == value) return;

                islisten_ListenEmail = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_ListenEmail);

            }
        }

        private bool islisten_ListenFax;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Button, ViewItemId = "isListenFax", ViewItemType = ViewItemType.Checked)]
		public bool IsListen_ListenFax
        {
            get { return islisten_ListenFax; }
            set
            {
                if (islisten_ListenFax == value) return;

                islisten_ListenFax = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_IsListen_ListenFax);

            }
        }

        private Dictionary<string, object> dictionary_NewKomItem;
        [ViewModel(Send = true, ViewItemClass = ViewItemClass.Grid, ViewItemId = "comGrid", ViewItemType = ViewItemType.AddRow)]
		public Dictionary<string, object> Dictionary_NewKomItem
        {
            get { return dictionary_NewKomItem; }
            set
            {
                if (dictionary_NewKomItem == value) return;

                dictionary_NewKomItem = value;

                RaisePropertyChanged(NotifyChanges.ViewModel_Dictionary_NewKomItem);

            }
        }
    }
}
